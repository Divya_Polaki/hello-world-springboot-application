FROM openjdk:8-jdk-alpine
RUN addgroup -S divyapolaki && adduser -S divyapolaki -G divyapolaki
USER divyapolaki:divyapolaki
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]